package com.example.toasty.slice;


import com.example.toasty.ResourceTable;
import es.dmoral.toasty.Toasty;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.components.element.PixelMapElement;
import ohos.agp.text.Font;
import ohos.agp.text.RichText;
import ohos.agp.text.RichTextBuilder;
import ohos.agp.text.TextForm;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;

import java.io.IOException;

public class MainAbilitySlice extends AbilitySlice {
    private Context mContext;
    private Button btn_one;

    private PixelMapElement pets = null;
    private PixelMapElement laptop512 = null;
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        mContext = getContext();
        initView();
    }

    private void initView(){
        try{
            ImageSource.SourceOptions sourceOptions = new ImageSource.SourceOptions();
            ImageSource source = ImageSource.create(getResourceManager().getResource(ResourceTable.Media_ic_pets_white_48dp),sourceOptions);
            ImageSource.DecodingOptions decodingOptions = new ImageSource.DecodingOptions();
            PixelMap pixelMap = source.createPixelmap(decodingOptions);
            pets = new PixelMapElement(pixelMap);
        }catch (IOException | NotExistException e){
            e.printStackTrace();
        }
        try{
            ImageSource.SourceOptions sourceOptions = new ImageSource.SourceOptions();
            ImageSource source = ImageSource.create(getResourceManager().getResource(ResourceTable.Media_laptop512),sourceOptions);
            ImageSource.DecodingOptions decodingOptions = new ImageSource.DecodingOptions();
            PixelMap pixelMap = source.createPixelmap(decodingOptions);
            laptop512 = new PixelMapElement(pixelMap);
        }catch (IOException | NotExistException e){
            e.printStackTrace();
        }
        btn_one = (Button)findComponentById(ResourceTable.Id_btn_normal_toast_intmsg);
        btn_one.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                Toasty.success(mContext,"testtesttesttesttest\ntest\ntesttesttesttesttesttest", Toasty.LENGTH_SHORT);
                Toasty.Config.reset();
            }
        });


        findComponentById(ResourceTable.Id_button_error_toast).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                Toasty.error(mContext,ResourceTable.String_error_message,Toasty.LENGTH_SHORT,true);
            }
        });
        findComponentById(ResourceTable.Id_button_success_toast).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                Toasty.success(mContext,ResourceTable.String_success_message,Toasty.LENGTH_SHORT,true);
            }
        });
        findComponentById(ResourceTable.Id_button_info_toast).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                Toasty.info(mContext,ResourceTable.String_info_message,Toasty.LENGTH_SHORT,true);
            }
        });

        findComponentById(ResourceTable.Id_button_warning_toast).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                Toasty.warning(mContext,ResourceTable.String_warning_message,Toasty.LENGTH_SHORT,true);
            }
        });
        findComponentById(ResourceTable.Id_button_normal_toast_wo_icon).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                Toasty.normal(mContext,ResourceTable.String_normal_message_without_icon);
            }
        });

        findComponentById(ResourceTable.Id_button_normal_toast_w_icon).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                Toasty.normal(mContext,ResourceTable.String_normal_message_with_icon,pets);
            }
        });
        findComponentById(ResourceTable.Id_button_info_toast_with_formatting).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                Toasty.info(mContext,getFormattedMessage());
            }
        });
        findComponentById(ResourceTable.Id_button_custom_config).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
//                Toasty.success(mContext,ResourceTable.String_normal_message_without_icon,Toasty.LENGTH_SHORT,true);
            }
        });
    }

    private RichText getFormattedMessage(){
        final String prefix = "Formatted";
        final String highlight = "bold italic";
        final String suffix = " text";

        TextForm textForm = new TextForm();
        TextForm textForm1 = new TextForm();

        Font.Builder builder = new Font.Builder("serif");
        builder.makeItalic(true);
        builder.setWeight(Font.BOLD);

        textForm1.setTextFont(builder.build());
        textForm1.setTextColor(0xffff0000);

        RichTextBuilder richTextBuilder = new RichTextBuilder(textForm);
        richTextBuilder.addText(prefix)
                .mergeForm(textForm1).addText(highlight)
                .revertForm().addText(suffix);
        return richTextBuilder.build();
    }

}
